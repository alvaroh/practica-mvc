<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="../../resources/main.css">
    <meta charset="UTF-8">
    <title>${titulo}</title>

</head>
<h1 class="titleDiv" style="color: black">
    Reto MR06
</h1>
<h1>
   Bienvenido a nuestra universidad
</h1>
<form method="get" action="/vista2">
    <label class="tdLabel" style="color: black">
        Elige una facultad:
    </label>
    <select name="universidad" id="universidades">
        <option value="null">---</option>
        <c:forEach items="${facultades}" var="facultad">
            <option value="${facultad.id}">${facultad.name}</option>
        </c:forEach>
    </select>
    <br>
    <input type="submit" value="Ver Grados">
</form>

</html>