<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="../../resources/main.css">
    <meta charset="UTF-8">
    <title>${titulo}</title>

</head>
<h1 class="titleDiv" style="color: black">
    Reto MR06
</h1>
<h1>
   Grados de Facultad ${facultad.id}
</h1>

<table class="borderAll">
    <tr class="">
        <td class="tdLabel">Código</td>
        <td class="tdLabel">Título</td>
        <td class="tdLabel">Créditos</td>
    </tr>

    <c:forEach items="${grados}" var="grado" varStatus="loop">
        <tr>
            <td ${loop.index % 2 != 0 ? "class = 'even'" : ""}>${grado.id}</td>
            <td ${loop.index % 2 != 0 ? "class = 'even'" : ""}>${grado.name}</td>
            <td ${loop.index % 2 != 0 ? "class = 'even'" : ""}>${grado.ects}</td>
        </tr>
    </c:forEach>
</table>
<a href="/vista1">Volver</a>

</html>