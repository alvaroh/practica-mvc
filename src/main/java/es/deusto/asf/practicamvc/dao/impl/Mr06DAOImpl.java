package es.deusto.asf.practicamvc.dao.impl;

import es.deusto.asf.practicamvc.dao.Mr06DAO;
import es.deusto.asf.practicamvc.model.Faculty;
import es.deusto.asf.practicamvc.model.Grade;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionException;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Reader;
import java.util.List;


public class Mr06DAOImpl implements Mr06DAO {

	private SqlSessionFactory sqlSessionFactory;

	public Mr06DAOImpl() {
		try {
			String resource = "mybatis-cfg.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Faculty> findAllFaculties() {
		SqlSession session = sqlSessionFactory.openSession();
		List<Faculty> faculties = session.selectList("getFaculties");
		session.close();

		return faculties;
	}

	public Faculty findFacultyById(String id) {
		SqlSession session = sqlSessionFactory.openSession();
		Faculty fac = session.selectOne("getFaculty", id);
		session.close();
		
		return fac;
	}

	public List<Grade> findGradesByFaculty(String faculty){
		SqlSession session = sqlSessionFactory.openSession();
		List<Grade> grades = session.selectList("getGradesByFaculty", faculty);
		session.close();

		return grades;
	}
	
	public void insertFaculty(Faculty faculty){
		SqlSession session = sqlSessionFactory.openSession();
		try{
			session.insert("insertFaculty", faculty);
			session.commit();
		}catch(SqlSessionException e){
			session.rollback();
			throw e;
		}finally{
			session.close();
		}
	}
	
	public void deleteFaculty(String id){
		SqlSession session = sqlSessionFactory.openSession();
		try{
			session.delete("deleteFaculty", id);
			session.commit();
		}catch(SqlSessionException e){
			session.rollback();
			throw e;
		}finally{
			session.close();
		}
	}
}
