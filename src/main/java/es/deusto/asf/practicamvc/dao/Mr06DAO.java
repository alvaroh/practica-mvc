package es.deusto.asf.practicamvc.dao;

import es.deusto.asf.practicamvc.model.Faculty;
import es.deusto.asf.practicamvc.model.Grade;

import java.util.List;

public interface Mr06DAO {
	public List<Faculty> findAllFaculties();
	public List<Grade> findGradesByFaculty(String faculty);
	public Faculty findFacultyById(String id);
	
	public void insertFaculty(Faculty faculty);
	public void deleteFaculty(String id);
}
