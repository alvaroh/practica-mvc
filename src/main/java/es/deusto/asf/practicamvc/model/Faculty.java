package es.deusto.asf.practicamvc.model;

import java.util.Set;

public class Faculty {
	private String id;
	private String name;
	private String url;
	
	public Faculty() {
	}	
	public Faculty(String id, String name, String url) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
