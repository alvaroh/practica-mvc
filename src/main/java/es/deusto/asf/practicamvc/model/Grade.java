package es.deusto.asf.practicamvc.model;

public class Grade {
	private String id;
	private String name;
	private int ects;
	
	public Grade() {	
	}
	public Grade(String id, String name, int ects) {
		super();
		this.id = id;
		this.name = name;
		this.ects = ects;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEcts() {
		return ects;
	}
	public void setEcts(int ects) {
		this.ects = ects;
	}
}
