package es.deusto.asf.practicamvc;


import es.deusto.asf.practicamvc.dao.Mr06DAO;
import es.deusto.asf.practicamvc.dao.impl.Mr06DAOImpl;
import es.deusto.asf.practicamvc.model.Faculty;
import es.deusto.asf.practicamvc.model.Grade;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static org.apache.logging.log4j.LogManager.getLogger;

@Controller
class PruebaController {

    private static final Logger LOG = getLogger(PruebaController.class.getName());

    Mr06DAO mr06DAO = new Mr06DAOImpl();

    @GetMapping("/vista1")
    public String mostrarPrueba(Model model){

        LOG.log(Level.INFO, "Petición recibida en /vista normal");

        try {
            List<Faculty> facultades = mr06DAO.findAllFaculties();
            model.addAttribute("facultades", facultades);

        } catch (Exception e) {
            LOG.log(Level.ERROR, "No se ha recibido ningún ID");
        }

        return "vista1";

    }

    @GetMapping("/vista2")
    public String mostrarFacultad(@RequestParam(name="universidad", required=false) String universidad, Model model){

        LOG.log(Level.INFO, "Petición recibida en /vista2");

        try {
            Faculty facultad = mr06DAO.findFacultyById(universidad);
            model.addAttribute("facultad", facultad);

            List<Grade> grados = mr06DAO.findGradesByFaculty(facultad.getId());
            model.addAttribute("grados", grados);

        } catch (Exception e) {
            LOG.log(Level.ERROR, "No se ha recibido ningún ID");
            return mostrarPrueba(model);
        }

        return "vista2";

    }



}
